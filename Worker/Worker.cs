﻿using System;
using System.Threading;

namespace DelannoyNumbers.Worker
{
    public class Worker
    {
        private static readonly int MMax = 1000;
        private static readonly int NMax = 1000;

        private readonly DelannoyNumbers _delannoyNumbers = new DelannoyNumbers(MMax, NMax);

        public int Delay
        {
            get => _delay;
            private set
            {
                _delay = value;
                if (_hasStarted)
                    _t.Change(_delay, _delay);
            }
        }

        public event EventHandler<ComputationCompleteEventArgs> ComputationComplete;

        private TimerCallback _timeCb;
        private Timer _t;
        private Random _rand = new Random();

        private bool _hasStarted = false;
        private int _delay;

        public Worker(int delay, IDemonstrator d)
        {
            Delay = delay;
            d.ComputationsStarted += OnComputationsStarted;
            d.ComputationsStopped += OnComputationsStopped;
            d.DelayChanged += OnDelayChanged;
        }
        
        void OnComputationsStarted(object sender, EventArgs e)
        {
            if (!_hasStarted)
            {
                _hasStarted = true;
                _timeCb = ComputeRandom;
                _t = new Timer(
                    _timeCb,
                    null,
                    Delay,
                    Delay);
            }
        }

        private void OnComputationsStopped(object? sender, EventArgs e)
        {
            _t.Dispose();
            _hasStarted = false;
        }

        private void OnDelayChanged(object? sender, DelayChangedEventArgs e)
        {
            Delay = e.Delay;
        }

        void ComputeRandom(object state)
        {
            Compute(_rand.Next() % MMax, _rand.Next() % NMax);
        }

        void Compute(int m, int n)
        {
            var result = _delannoyNumbers[m, n];
            ComputationComplete?.Invoke(this, new ComputationCompleteEventArgs
            {
                M = m,
                N = n,
                Result = result
            });
        }
    }
}