﻿using System;
using System.Windows.Input;
using DelannoyNumbers.Worker;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace DelannoyNumbers.Demonstrator
{
    public class MainWindowViewModel : ViewModelBase, IDemonstrator
    {
        private string _workerResult = "computation hasn't been done yet";

        private int _currentDelay = 1000;

        public int CurrentDelay
        {
            get => _currentDelay;
            set
            {
                _currentDelay = value;
                DelayChanged?.Invoke(this, new DelayChangedEventArgs {Delay = _currentDelay});
                RaisePropertyChanged(nameof(CurrentDelay));
            }
        }

        public string WorkerResult
        {
            get => _workerResult;
            set
            {
                _workerResult = value;
                RaisePropertyChanged(nameof(WorkerResult));
            }
        }

        private Worker.Worker _worker;

        public event EventHandler ComputationsStarted;
        public event EventHandler ComputationsStopped;
        public event EventHandler<DelayChangedEventArgs> DelayChanged;

        private string _buttonText;

        public string ButtonText
        {
            get => _buttonText;
            set
            {
                _buttonText = value;
                RaisePropertyChanged(nameof(ButtonText));
            }
        }

        private ICommand _currentCommand;

        public ICommand CurrentCommand
        {
            get => _currentCommand;
            set
            {
                _currentCommand = value;
                RaisePropertyChanged(nameof(CurrentCommand));
            }
        }

        public ICommand StopCommand { get; }
        public ICommand StartCommand { get; }

        public MainWindowViewModel()
        {
            StopCommand = new RelayCommand(OnStopCommand);
            StartCommand = new RelayCommand(OnStartCommand);
            _worker = new Worker.Worker(CurrentDelay, this);
            _worker.ComputationComplete += OnComputationComplete;
            CurrentCommand = StartCommand;
            CurrentCommand.Execute(null);
        }

        private void OnStartCommand()
        {
            ComputationsStarted?.Invoke(this, new EventArgs());

            ButtonText = "Stop";
            CurrentCommand = StopCommand;
        }

        private void OnStopCommand()
        {
            ComputationsStopped?.Invoke(this, new EventArgs());

            ButtonText = "Start";
            CurrentCommand = StartCommand;
        }

        private void OnComputationComplete(object sender, ComputationCompleteEventArgs e)
        {
            WorkerResult = $"m = {e.M},\nn = {e.N},\nresult = {e.Result},\ndelay = {((Worker.Worker) sender).Delay}.";
        }
    }
}